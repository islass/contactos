package com.example.cruz.contactos;

import android.app.Activity;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.loopj.android.http.Base64;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Laura on 24/02/2019.
 */

public class ListaPersonalizada extends ArrayAdapter<Contactos> {

    ArrayList<Contactos> contactos;
    LayoutInflater vi;
    int Resource;

    public ListaPersonalizada(Context context, int resource, ArrayList<Contactos> contactos) {
        super(context, resource, contactos);
        vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        Resource = resource;
        this.contactos = contactos;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 500;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // convert view = design
        View v = convertView;
        final ViewHolder holder;

        if (v == null) {
            v = vi.inflate(R.layout.listapersonalizada, null);
            holder = new ViewHolder();
            holder.foto = v.findViewById(R.id.foto);
            holder.texto = v.findViewById(R.id.tvtexto);
            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }
        holder.texto.setText(contactos.get(position).getNomnbre() + "\n"+contactos.get(position).Telefono );
        byte[] fotoBytes;
        String foto =contactos.get(position).getFoto();
        holder.foto.setImageResource(R.drawable.contacto);
        if (foto!=null ) {
            if (!foto.equals("")) {
                String fotobase64 = contactos.get(position).getFoto();
                if (!fotobase64.equals(null)) {
                    fotoBytes = Base64.decode(fotobase64, Base64.DEFAULT);
                    holder.foto.setImageBitmap(BitmapFactory.decodeByteArray(fotoBytes, 0, fotoBytes.length));
                }
            }
        } else {
            holder.foto.setImageResource(R.drawable.contacto);
        }
        return v;
    }

    private static class ViewHolder {

        public ImageView foto;
        public TextView texto;

    }
}
