package com.example.cruz.contactos;

import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.RequiresApi;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

import cz.msebera.android.httpclient.entity.mime.MultipartEntityBuilder;

public class PostContactos extends  AsyncTask<Void,Void,Void> {

    public  String url;
    public String nombre,direccion;

    public PostContactos(String url, String nombre, String direccion) {
        this.url = url;
        this.nombre = nombre;
        this.direccion = direccion;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected Void doInBackground(Void... voids) {
        try {

            URL uri = new URL(url);
            //
            JSONObject contacto = new JSONObject();
            contacto.put("nombre",nombre);
            contacto.put("direccion",direccion);
            String json= contacto.toString();

            //
           byte[] out = "{\"nombre\":\"root\",\"direccion\":\"5 de mayo\"}" .getBytes(StandardCharsets.UTF_8);

            HttpURLConnection conexion = (HttpURLConnection) uri.openConnection();
            conexion.setDoOutput(true);
            conexion.setRequestMethod("POST");

            conexion.setFixedLengthStreamingMode(out.length);
            conexion.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            conexion.connect();
            OutputStreamWriter salida = new OutputStreamWriter(conexion.getOutputStream());
            salida.write(json);// here i sent the parameter









            //conexion.getOutputStream().write(json.getBytes());




            //InputStream entradaDatos = conexion.getInputStream();
            //BufferedReader reader = new BufferedReader(new InputStreamReader(entradaDatos));

           /* String linea="";
            StringBuilder respuesta = new StringBuilder();
            while ((linea = reader.readLine()) != null)
                respuesta.append(linea);

            //JSONArray contacto = new JSONArray(respuesta.toString());

            ArrayList<String> chichi = new ArrayList<>();
          /*  for (int g = 0; g < contacto.length(); g++){
                String contactoTemp;
                JSONObject objetoContacto = contacto.getJSONObject(g);
                JSONArray telefonos= new JSONArray(objetoContacto.getString("telefonos"));
                contactoTemp=objetoContacto.getString("nombre");
                for (int i=0;i<telefonos.length();i++){
                    JSONObject objetoTelefonos = telefonos.getJSONObject(i);
                    contactoTemp+="\n Telefono["+i+"]:" + objetoTelefonos.getString("numero");
                }
                chichi.add(contactoTemp);

            }
            */
            //contactos.clear();
            //contactos.addAll(chichi);

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return  null;
    }

}
