package com.example.cruz.contactos;

import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.Response;
import com.android.volley.Request;
import com.android.volley.toolbox.Volley;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONException;
import java.util.ArrayList;



public class MainActivity extends AppCompatActivity {

    TextView tvDatos;
    RequestQueue requestQueue;
    ListView lvContactos;
    Button btnActualizar, btnAgreagarnuevo;

    static String baseUrl = "http://192.168.8.105:8080/api/values";  // Esta es la URL de la API
   // static String baseUrl="http://192.168.43.100:8080/api/values";

    //ArrayList<Contactos> contactos= new ArrayList<>();
    ArrayList<String> contactos= new ArrayList<>();
    ArrayAdapter adaptador;
    //ListaPersonalizada adaptador;
    ImageView foto;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tvDatos = findViewById(R.id.tvDatos);
        tvDatos.setMovementMethod(new ScrollingMovementMethod());
        lvContactos = findViewById(R.id.lvContactos);
        btnActualizar = findViewById(R.id.btnActualizar);
        btnAgreagarnuevo = findViewById(R.id.btnAgregarNuevo);
        requestQueue = Volley.newRequestQueue(this);  // Esto configura una nueva cola de solicitudes que necesitaremos para realizar solicitudes HTTP.

        //getList("adrianbranchbit");
        adaptador= new ArrayAdapter(this,android.R.layout.simple_expandable_list_item_1,contactos);
        //adaptador= new ListaPersonalizada(this,R.layout.listapersonalizada,contactos);
        lvContactos.setAdapter(adaptador);

        //new GetContacto(baseUrl, contactos, adaptador,foto).execute();
        new GetContacto(baseUrl,contactos,adaptador).execute();


        btnActualizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new GetContacto(baseUrl,contactos,adaptador).execute();
            }
        });
        btnAgreagarnuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), AgregarNuevoContacto.class);
                startActivity(intent);
            }
        });

    }

    public void clrearList() {
        tvDatos.setText(" ");
    }
    private void getList() {
        //url= baseUrl+username+"/repos";

        JsonArrayRequest arr = new JsonArrayRequest(Request.Method.GET, baseUrl, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                if (response.length() > 0) {

                    for (int i = 0; i < response.length(); i++) {
                        try {
                            // For each repo, add a new line to our repo list.
                            JSONObject jsonObj = response.getJSONObject(i);
                            String contacto = "Nombre: " + jsonObj.get("nombre").toString();
                            contacto += "\nTelefono: " + jsonObj.get("telefono").toString();

                            addToRepoList(contacto);
                        } catch (JSONException e) {
                            // If there is an error then output this to the logs.
                            Log.e("Volley", "Invalid JSON Object.");
                        }

                    }
                } else {
                    // The user didn't have any repos.
                }
                ArrayAdapter contactosAdapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_list_item_1, contactos);
                lvContactos.setAdapter(contactosAdapter);
            }
        },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("Volley", error.toString());
                    }
                });
        requestQueue.add(arr);

    }
    private void addToRepoList(String repoName) {
        //contacto.add((repoName));
        //String currentText = tvDatos.getText().toString();
        //this.tvDatos.setText(currentText + "\n\n" + strRow);
    }


}
