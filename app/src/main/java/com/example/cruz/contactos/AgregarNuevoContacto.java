package com.example.cruz.contactos;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Laura on 21/02/2019.
 */

public class AgregarNuevoContacto extends AppCompatActivity {
    EditText etNombre, etdireccion;
    Button btnRegistrar;
    RequestQueue request;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.agregarnuevocontacto);
        etdireccion = findViewById(R.id.etDireccion);
        etNombre = findViewById(R.id.etNombre);
        btnRegistrar = findViewById(R.id.btnRegistrar);
        request = Volley.newRequestQueue(this);



        btnRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                StringRequest MyStringRequest = new StringRequest(Request.Method.POST, MainActivity.baseUrl, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //This code is executed if the server responds, whether or not the response contains data.
                        //The String 'response' contains the server's response.
                    }
                }, new Response.ErrorListener() { //Create an error listener to handle errors appropriately.
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //This code is executed if there is an error.
                    }
                }) {
                    protected Map<String, String> getParams() {
                        Map<String, String> MyData = new HashMap<>();
                        MyData.put("nombre", etNombre.getText().toString()); //Add the data you'd like to send to the server.
                        MyData.put("direccion", etdireccion.getText().toString()); //Add the data you'd like to send to the server.
                        return MyData;
                    }
                };
                request.add(MyStringRequest);
                Toast.makeText(getApplicationContext(),"Registrop exitoso",Toast.LENGTH_SHORT).show();
                etNombre.setText("");
                etdireccion.setText("");
            }
        });
    }

}
